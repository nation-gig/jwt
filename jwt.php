<?php

function base64UrlEncode($input){
    return str_replace(
        ['+', '/', '='],
        ['-', '_', ''],
        base64_encode($input)
    );
}

function base64UrlDecode($input){
	return str_replace(
		['-', '_', ''],
        ['+', '/', '='],
        base64_decode($input)
    );
}

function signInput($input,$key,$alg = 'sha256'){
	return hash_hmac($alg, $input, $key, true);
}

function decodeClaims($data){
	return json_decode(base64UrlDecode($data));
}

function verifySignature($data,$signature,$key,$alg = 'sha256'){
	$hash = hash_hmac($alg, $data, $key, true);

    if (function_exists('hash_equals')) {
        return hash_equals($signature,$hash);
    }

    return hash_compare($hash,$signature);
}

function hash_compare($hash, $signature) {
    if (!is_string($hash) || !is_string($signature)) {
        return false;
    }
   
    $len = \min(safeStrlen($signature), safeStrlen($hash));

    $status = 0;
    for ($i = 0; $i < $len; $i++) {
        $status |= (\ord($signature[$i]) ^ \ord($hash[$i]));
    }
    $status |= (safeStrlen($signature) ^ safeStrlen($hash));

    return ($status === 0);
}

function safeStrlen($str)
{
    if (\function_exists('mb_strlen')) {
        return \mb_strlen($str, '8bit');
    }
    return \strlen($str);
}

/**
* Get hearder Authorization
* */
function getAuthorizationHeader(){
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    }
    return $headers;
}

/**
 * get access token from header
 * */
function getBearerToken() {
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    throw new Exception('Access Token Not found');
}

function generateJwt($payload,$key){
	// creating the token header
	$header = json_encode([
		'type' => 'JWT',
		'alg' => 'HS256'
	]);

	// Encode Header
	$base64UrlHeader = base64UrlEncode($header);

	// Encode Payload
	$base64UrlPayload = base64UrlEncode(json_encode($payload));

	$signingInput = $base64UrlHeader.".".$base64UrlPayload;
	$signature = signInput($signingInput,$key);
	// Create Signature Hash
	$base64UrlSignature = base64UrlEncode($signature);

	// Create JWT
	return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
}

function validateJwt($jwt, $key){
	$leewayTime = 60; // (time in seconds) useful for the time server and script loading difference 
	$timestamp = time();
    if (empty($key)) {
        throw new Exception('Key can not be empty');
    }

    $token = explode('.', $jwt);
    if (count($token) != 3) {
        throw new Exception('Wrong number of segments');
    }

    list($b64Header, $b64Payload, $b64Signature) = $token;
    // validating each segments of the token
    if(null === ($header = decodeClaims($b64Header))){
    	throw new Exception('Invalid header encoding');
    }

    if(null === $payload = decodeClaims($b64Payload)){
    	throw new Exception('Invalid claims encoding');
    }

    if (false === ($sig = base64UrlDecode($b64Signature))) {
        throw new Exception('Invalid signature encoding');
    }

    if (empty($header->alg)) {
        throw new Exception('The algorithm is empty');
    }

    // checking the signature
    $signInput = $b64Header.".".$b64Payload;
    if (!verifySignature($signInput, $sig, $key)) {
        throw new Exception('Signature verification failed');
    }

    // Check that this token has been created before 'now'. This prevents
    // using tokens that have been created for later use.
    if (isset($payload->iat) && $payload->iat > ($timestamp + $leewayTime)) {
        throw new Exception(
            'Cannot handle token prior to ' . date(DateTime::ISO8601, $payload->iat)
        );
    }
    // Check if this token has expired.
    if (isset($payload->exp) && ($timestamp - $leewayTime) >= $payload->exp) {
        throw new Exception('It seems the token has expired');   
    }
    return $payload;

}

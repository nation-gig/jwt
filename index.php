<?php
use Firebase\JWT\JWT;
 require_once('jwt.php');

// // generating a secret key
$secret = bin2hex(random_bytes(20));

// // creating the token payload
// $payload = [
// 	'iat' => time(),
// 	'iss' => 'localhost',
// 	'exp' => time() + (15*60),
// 	'param' => [
// 		'ID' => 1,
// 		'username' => 'holynation',
// 	]
// ];

// echo "SECRET KEY: ".$secret . "<br>";
// $testToken = generateJwt($payload, $secret);
// echo "TOKEN: ". $testToken;

// // $testToken = "eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJpYXQiOjE1OTgwODYwODAsImlzcyI6ImxvY2FsaG9zdCIsImV4cCI6MTU5ODA4Njk4MCwicGFyYW0iOnsiSUQiOjEsInVzZXJuYW1lIjoiaG9seW5hdGlvbiJ9fQ.JQWXJGILjJr8XVtQwxjvXdPjE0paKY3_h_YHGvgoLvI";
// // $equivalentSecret = "f2d2db3fa064882d17b45e83bfb312120dd4ac09";

// // /* 
// // this will be used if the token is sent as authentication in the header
// // */
// // // $testToken = getBearerToken();

// try{
// 	validateJwt($testToken,$secret);
// }catch(Exception $e){
// 	echo $e->getMessage();
// }

require_once 'JWT.php';

$key = "example_key";
$payload = array(
    "iss" => "http://example.org",
    "aud" => "http://example.com",
    "iat" => 1356999524,
    "nbf" => 1357000000
);

$jwt = JWT::encode($payload, $secret);
$decoded = JWT::decode($jwt, $secret, array('HS256'));

print_r($decoded);
